:orphan:

General
============

.. py:currentmodule:: peegy

.. toctree::

General pipeline processing InputOutput classes
-----------------------------------------------

.. py:currentmodule:: peegy
.. automodule:: peegy.processing.pipe.general
   :no-members:
   :no-inherited-members:

.. autosummary::
   :toctree: generated/

   FilterData
   ReSampling
   RemoveBadChannels
   AutoRemoveBadChannels
   ReferenceData
   RegressOutEOG
   BaselineCorrection



