:orphan:

Epochs
============

.. py:currentmodule:: peegy

.. toctree::

Specific epoch based pipeline processing InputOutput classes
------------------------------------------------------------

.. py:currentmodule:: peegy
.. automodule:: peegy.processing.pipe.epochs
   :no-members:
   :no-inherited-members:

.. autosummary::
   :toctree: generated/

   AverageEpochs
   EpochData
   RejectEpochs
   SortEpochs
   AverageEpochsFrequencyDomain
   DeEpoch
   RemoveBadChannelsEpochsBased
