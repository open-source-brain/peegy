:orphan:

Storage
============

.. py:currentmodule:: peegy

.. toctree::

Specific storage pipeline processing InputOutput classes
------------------------------------------------------------

.. py:currentmodule:: peegy
.. automodule:: peegy.processing.pipe.storage
   :no-members:
   :no-inherited-members:

.. autosummary::
   :toctree: generated/

   SaveToDatabase
